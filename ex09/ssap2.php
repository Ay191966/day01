#!/usr/bin/env php
<?php
function ssap($var1, $var2)
{
    $i = -1;
	while($var1[++$i] || $var2[++$i])
	{
		if (ctype_alpha($var1[$i]))
		{
			if (ctype_alpha($var2[$i]))
			{
				if (strcmp(strtolower($var1[$i]), strtolower($var2[$i])) == 0)
					continue ;
				return (strcmp(strtolower($var1[$i]), strtolower($var2[$i])));
			}
			return (-1);
		}
		else if (is_numeric($var1[$i]))
		{
			if (ctype_alpha($var2[$i]))
				return (1);
			else if (is_numeric($var2[$i]))
				return (strcmp($var1[$i], $var2[$i]));
			return (-1);
		}
		else
		{
			if (!ctype_alpha($var2[$i]) && !is_numeric($var2[$i]))
				return (strcmp($var1[$i], $var2[$i]));
			return (1);
		}
	}
}
if ($argc > 1)
{
	$arr = [];
	for ($i = 1; $i < $argc; $i++)
	{
		$str = trim($argv[$i]);
		$arr = array_merge($arr, array_filter(explode(" ", $str), "strlen"));
    }
	usort($arr, "ssap");
	foreach ($arr as $str)
		echo $str."\n";
}
?>