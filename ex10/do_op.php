#!/usr/bin/php
<?php
    if(!is_numeric($argv[1]) || $argc != 4 || !is_numeric($argv[3]))
        echo "Incorrect Parameters";
    $argv[1] = trim($argv[1]);
    $argv[2] = trim($argv[2]);
    $argv[3] = trim($argv[3]);
    if($argv[2] == "*")
        echo $argv[1] * $argv[3];
    else if($argv[2] == "+")
        echo $argv[1] + $argv[3];
    else if($argv[2] == "-")
        echo $argv[1] - $argv[3];
    else if($argv[2] == "/" && $argv[3] != '0')
        echo $argv[1] / $argv[3];
    else if($argv[2] == "%")
        echo $argv[1] % $argv[3];

?>