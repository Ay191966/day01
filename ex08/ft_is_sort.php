#!/usr/bin/php
<?php
function ft_is_sort($array){
	$asort = $array;
	$dsort = $array;
	sort($asort,SORT_STRING);
	rsort($dsort);
	if (array_diff_assoc($asort, $array) == null || array_diff_assoc($dsort, $array) == null)
		return true;
	return false;
}
?>